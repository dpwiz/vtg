{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}

module Example.JSON
  ( JSON, JSON'
  , Nums
  , NullF(..), pattern Null, null
  , LitF(..), pattern Lit, lit, litNum, litStr, litBool
  , ArrayF(..), pattern Array, array
  , DictF(..), pattern Dict, dict
  ) where

import Prelude hiding (null)

import Data.Text (Text)
import Data.Tree (Tree(..))
import Data.Scientific (Scientific)
import Data.Typeable (Typeable, typeOf)

import VTG.Compiler
  ( Pass(..), NoExt(..)
  , TreeView1(..)
  , EADT, (:<:), pattern VF
  )

import VTG.Compiler.Typing

-- * Constuctors

-- ** Null

data NullF x r = NullF x
  deriving (Functor, Show)

pattern Null
  :: (NullF x :<: types)
  => x -> EADT types
pattern Null x = VF (NullF x)

null :: (NullF NoExt :<: types) => EADT types
null = Null NoExt

instance TreeView1 (NullF x) where
  treeViewLabel1 _ = "Null"
  treeViewNodes1 _ = mempty

instance
  ( TConsF :<: types
  ) => HasType types (NullF x r)
  where
    getType _ =
      VF (TConsF "Null")

-- ** Literals

data LitF x a r = LitF x a
  deriving (Functor, Show)

pattern Lit
  :: (LitF x a :<: types)
  => x -> a -> EADT types
pattern Lit x a = VF (LitF x a)

lit :: (LitF NoExt a :<: types) => a -> EADT types
lit = Lit NoExt

litBool :: (LitF NoExt Bool :<: types) => Bool -> EADT types
litBool = lit

litNum :: (LitF NoExt Scientific :<: types) => Scientific -> EADT types
litNum = lit

litStr :: (LitF NoExt Text :<: types) => Text -> EADT types
litStr = lit

instance (Show a, Typeable a) => TreeView1 (LitF x a) where
  treeViewLabel1 (LitF _ a) =
    "Lit " ++ show (typeOf a)

  treeViewNodes1 (LitF _ a) =
    [ Node (show a) mempty
    ]

instance
  ( TConsF :<: types
  , Typeable a
  ) => HasType types (LitF x a r)
  where
    getType (LitF _ a) = fromTypeable a

-- ** Expr-unityped Array

data ArrayF x r = ArrayF x [r]
  deriving (Functor, Show)

pattern Array
  :: (ArrayF x :<: types)
  => x -> [EADT types] -> EADT types
pattern Array x r = VF (ArrayF x r)

array :: (ArrayF NoExt :<: types) => [EADT types] -> EADT types
array = Array NoExt

type family XArray (pass :: Pass) where
  XArray 'Parsed    = NoExt
  XArray 'Resolved  = NoExt
  XArray 'Generated = NoExt

instance TreeView1 (ArrayF x) where
  treeViewLabel1 _ = "Array"
  treeViewNodes1 (ArrayF _ xs) = xs

instance
  ( TConsF :<: types
  ) => HasType types (ArrayF x r)
  where
    getType _ =
      VF (TConsF "Array")

-- ** Expr-unityped Dict

data DictF x r = DictF x [(Text, r)]
  deriving (Functor, Show)

pattern Dict
  :: (DictF x :<: types)
  => x -> [(Text, EADT types)] -> EADT types
pattern Dict x r = VF (DictF x r)

dict :: (DictF NoExt :<: types) => [(Text, EADT types)] -> EADT types
dict = Dict NoExt

type family XDict (pass :: Pass) where
  XDict 'Parsed    = NoExt
  XDict 'Resolved  = NoExt
  XDict 'Generated = NoExt

instance TreeView1 (DictF x) where
  treeViewLabel1 _ = "Dict"

  treeViewNodes1 (DictF _ pairs) =
    [ Node (show key) [value]
    | (key, value) <- pairs
    ]

instance
  ( TConsF :<: types
  ) => HasType types (DictF x r)
  where
    getType _ =
      VF (TConsF "Dict")

-- * Combined expressions

-- ** Number arrays (and arrays of that)

type Nums = EADT
  '[ LitF   NoExt Scientific -- 4.2e1
   , ArrayF NoExt            -- [ 4.2e1, 2.71, 3.14 ]
   ]

-- **  Classic JSON term

type JSON x = EADT (JSON' x)

type JSON' x =
  '[ NullF  (XJSON x)            -- null
   , LitF   (XJSON x) Bool       -- "лꙐжи"
   , LitF   (XJSON x) Scientific -- 4.2e1
   , LitF   (XJSON x) Text       -- "лꙐжи"
   , ArrayF (XJSON x)            -- [ null, 4.2e1, "лꙐжи" ]
   , DictF  (XJSON x)            -- { "key": ["values"] }
   ]

-- XXX: JSON is unityped
type JSONTypes = '[ TConsF ]

type family XJSON (pass :: Pass) where
  XJSON 'Generated = NoExt
  XJSON 'Resolved  = EADT JSONTypes
