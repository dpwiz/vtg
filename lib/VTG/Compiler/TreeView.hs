{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}

module VTG.Compiler.TreeView
  ( displayTree
  , TreeView(..)
  , TreeView1(..)
  , Tree(..)
  , showTree
  ) where

import Data.Tree (Tree(..))
import Data.Tree.View (showTree)

import VTG.Compiler.Common
  ( Type
  , cata
  , Variant, popVariantHead
  , VariantF, popVariantFHead, EADT
  )

-- | Direct recursion trees
class TreeView a where
  treeView :: a -> Tree String
  treeView = Node <$> treeViewLabel <*> treeViewNodes
  {-# INLINE treeView #-}

  treeViewLabel :: a -> String
  treeViewLabel = mempty

  treeViewNodes :: a -> [Tree String]
  treeViewNodes = mempty

  {-# MINIMAL treeViewLabel, treeViewNodes | treeView #-}

instance TreeView (Variant '[]) where
  treeView = undefined

instance
  ( TreeView c
  , TreeView (Variant cs)
  ) => TreeView (Variant (c ': cs)) where
  treeView = either treeView treeView . popVariantHead

-- | Fixed-point trees
class TreeView1 (f :: Type -> Type) where
  treeViewF :: f (Tree String) -> Tree String
  treeViewF = Node <$> treeViewLabel1 <*> treeViewNodes1
  {-# INLINE treeViewF #-}

  treeViewLabel1 :: f (Tree String) -> String
  treeViewNodes1 :: f (Tree String) -> [Tree String]

instance
  ( Functor (VariantF cs)
  , TreeView1 (VariantF cs)
  ) => TreeView (EADT cs)
  where
  treeView = cata treeViewF

instance TreeView1 (VariantF '[]) where
  treeViewLabel1 = undefined
  treeViewNodes1 = undefined

instance (TreeView1 c, TreeView1 (VariantF cs)) => TreeView1 (VariantF (c ': cs)) where
  treeViewLabel1 = either treeViewLabel1 treeViewLabel1 . popVariantFHead
  treeViewNodes1 = either treeViewNodes1 treeViewNodes1 . popVariantFHead

displayTree :: TreeView t => t -> IO ()
displayTree = putStrLn . showTree . treeView
