{-# LANGUAGE DataKinds #-}
{-# LANGUAGE UndecidableInstances #-}

module Example.Arith
  ( ValF(..), pattern Val, val, XVal
  , AddF(..), pattern Add, add, XAdd
  , MulF(..), pattern Mul, mul, XMul
  , EvalInt(..), AllEvalInt, evalIntEADT
  , IntSums
  , IntMults
  ) where

import VTG.Compiler
  ( Pass(..), NoExt(..)
  , TreeView1(..)
  , EADT, VariantF, popVariantFHead, (:<:), pattern VF
  , unfix
  )
import VTG.Compiler.Typing
  ( HasType(..)
  , TConsF
  , TVarF
  , TArrowF, binary
  , Typeable, fromTypeable
  )

-- * Operations

-- ** Expression evaluation with direct recursion

class EvalInt e where
  evalInt :: e -> Int

instance EvalInt (VariantF '[] e) where
  evalInt = undefined

instance (EvalInt (c e), EvalInt (VariantF cs e)) => EvalInt (VariantF (c ': cs) e) where
  evalInt var = either evalInt evalInt (popVariantFHead var)

type AllEvalInt cs = EvalInt (VariantF cs (EADT cs))

evalIntEADT :: AllEvalInt cs => EADT cs -> Int
evalIntEADT = evalInt . unfix

-- * Constuctors

-- ** Shallow-embeded literal

data ValF x a r = ValF x a
  deriving (Functor, Show)

pattern Val :: (ValF x a :<: types) => x -> a -> EADT types
pattern Val x a = VF (ValF x a)

val :: (ValF NoExt a :<: types) => a -> EADT types
val = Val NoExt

type family XVal (pass :: Pass) where
  XVal 'Parsed    = NoExt
  XVal 'Resolved  = NoExt
  XVal 'Generated = NoExt

instance (Show a) => TreeView1 (ValF x a) where
  treeViewLabel1 (ValF _ a) = show a

  treeViewNodes1 _ = mempty

instance
  ( Typeable a
  , TConsF :<: types
  ) => HasType types (ValF x a r)
  where
    getType (ValF _ a) =
      fromTypeable a

instance EvalInt (ValF x Int r) where
  evalInt (ValF _ i) = i

-- ** Addition operation

data AddF x r = AddF x r r
  deriving (Functor, Show)

pattern Add :: (AddF x :<: xs) => x -> EADT xs -> EADT xs -> EADT xs
pattern Add x a b = VF (AddF x a b)

add :: (AddF NoExt :<: types) => EADT types -> EADT types -> EADT types
add = Add NoExt

type family XAdd pass where
  XAdd 'Parsed    = NoExt
  XAdd 'Resolved  = NoExt
  XAdd 'Generated = NoExt

instance TreeView1 (AddF x) where
  treeViewLabel1 _ = "(+)"

  treeViewNodes1 (AddF _ a b) = [a, b]

instance
  ( TArrowF :<: types
  , TVarF :<: types
  ) => HasType types (AddF x r)
  where
    getType _ = binary "a"

instance (AllEvalInt cs) => EvalInt (AddF x (EADT cs)) where
  evalInt (AddF _ a b) = evalIntEADT a + evalIntEADT b

instance (AllEvalInt cs) => EvalInt (MulF x (EADT cs)) where
  evalInt (MulF _ a b) = evalIntEADT a * evalIntEADT b

-- ** Multiplication operation

data MulF x r = MulF x r r
  deriving (Functor, Show)

pattern Mul :: (MulF x :<: xs) => x -> EADT xs -> EADT xs -> EADT xs
pattern Mul x a b = VF (MulF x a b)

mul :: (MulF NoExt :<: types) => EADT types -> EADT types -> EADT types
mul = Mul NoExt

type family XMul pass where
  XMul 'Parsed    = NoExt
  XMul 'Resolved  = NoExt
  XMul 'Generated = NoExt

instance TreeView1 (MulF x) where
  treeViewLabel1 _ = "(*)"

  treeViewNodes1 (MulF _ a b) = [a, b]

-- * Combined expressions

-- | Addition-only
type IntSums x = EADT
  '[ ValF (XVal x) Int
   , AddF (XAdd x)
   ]

-- | Addition and multiplication
type IntMults x = EADT
  '[ ValF (XVal x) Int
   , AddF (XAdd x)
   , MulF (XMul x)
   ]
