{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}

module Main where

import Prelude hiding (null)

import VTG.Compiler (Pass(..), liftEADT)
import VTG.Compiler.TreeView (displayTree)

import Example.Arith (IntSums, IntMults, val, add, mul, evalIntEADT)
import Example.JSON (JSON, Nums, null, litNum, litStr, array, dict)
import Example.JSE (JSE)

main :: IO ()
main = do
  putStrLn "\nsumXY:"
  displayTree sumXY
  putStrLn $ "evalIntEADT sumXY = " ++ show (evalIntEADT sumXY)

  putStrLn "\nmultXYZ:"
  displayTree multXYZ
  putStrLn $ "evalIntEADT multXYZ = " ++ show (evalIntEADT multXYZ)

  putStrLn "\nJSON:"
  displayTree combined

-- * Arith values

x42 :: IntSums 'Generated
x42 = val @Int 42

y1337 :: IntSums 'Generated
y1337 = val @Int 1337

sumXY :: IntSums 'Generated
sumXY = x42 `add` y1337

multXYZ :: IntMults 'Generated
multXYZ = liftEADT sumXY `mul` val @Int 10

-- * JSON values

j42 :: JSON 'Generated
j42 = litNum 4.2e1

jHello :: JSON 'Generated
jHello = litStr "hellow, orld"

-- XXX: Impossible. "Lit ()" is not a subtype of JSON
-- jUnit :: JSON 'Generated
-- jUnit = lit ()

jArray :: JSON 'Generated
jArray = array [null, j42, jHello]

jDict :: JSON 'Generated
jDict = dict
  [ ("null",   null)
  , ("number", j42)
  , ("string", jHello)
  , ("array",  jArray)
  ]

nums :: Nums
nums = array
  [ litNum 42
  , litNum 2.71
  , litNum 3.14
  -- , null -- XXX: impossible
  , array [] -- XXX: those are trees after all...
  ]

jse :: JSE 'Generated
jse = array xs `mul` litNum 5
  where
    xs = zipWith add as as
    as = map (litNum . fromInteger) [1 .. 5]

combined :: JSE 'Generated
combined = dict
  [ ("json", liftEADT jDict)
  , ("nums", liftEADT nums)
  , ("expr", jse)
  ]

-- * Type-checking

jse42 :: JSE 'Generated
jse42 = liftEADT j42

jseHello :: JSE 'Generated
jseHello = liftEADT jHello

jseTwice42 :: JSE 'Generated
jseTwice42 = jse42 `mul` litNum 2 `add` litNum 0

jseBadAdd :: JSE 'Generated
jseBadAdd = jse42 `add` liftEADT jseHello `mul` litNum 1
