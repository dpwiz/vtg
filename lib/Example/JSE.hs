{-# LANGUAGE DataKinds #-}

module Example.JSE
  ( JSE
  ) where

import VTG.Compiler (EADT)

import Example.Arith as Arith
import Example.JSON as JSON

type JSE' x =
  Arith.AddF (Arith.XAdd x) ':
  Arith.MulF (Arith.XMul x) ':
  JSON.JSON' x

type JSE x = EADT (JSE' x)
