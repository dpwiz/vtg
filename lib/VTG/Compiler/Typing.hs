{-# LANGUAGE AllowAmbiguousTypes #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE IncoherentInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE UndecidableInstances #-}

-- | Type inference and checking

module VTG.Compiler.Typing
  ( HasType(..)
  , TConsF(..), fromTypeable, Typeable
  , TCons1F(..)
  , TCons2F(..)
  , TVarF(..)
  , TArrowF(..), binary
  , TUnionF(..)

  , TypedF(..), Typed', Typed, TV
  , treeViewTypes
  , AnnTypes(..), annTypes
  , ShowType(..), showType
  ) where

import Data.Functor.Compose (Compose(..))
import Data.Text.Short (ShortText)
import Data.Typeable (Typeable, typeOf)
import Haskus.Utils.EADT (ApplyAll, pattern FV)
import Haskus.Utils.Variant (PopVariant, Member)

import VTG.Compiler.Common
  ( EADT, VariantF, popVariantFHead, (:<:), pattern VF
  , Fix(..), cata
  , Type
  )
import VTG.Compiler.TreeView

import qualified Data.Text.Short as ShortText

-- * Type representation

class HasType types a where
  getType :: a -> EADT types

-- ** Common forms

-- | Known type constructor
-- @ LiterallyHitler
data TConsF r = TConsF ShortText
  deriving (Eq, Functor, Show)

instance ShowType TConsF where
  showTypeF (TConsF name) = name

fromTypeable :: (Typeable a, TConsF :<: types) => a -> EADT types
fromTypeable a = VF (TConsF name)
  where
    name = ShortText.fromString . show $ typeOf a

-- | Known type constructor with one free argument
-- @ Maybe Hitler
data TCons1F r = TCons1F ShortText r
  deriving (Functor, Show)

instance ShowType TCons1F where
  showTypeF (TCons1F name a) = ShortText.intercalate " "
    [ name
    , a
    ]

-- | Known type constructor with two free argument
-- @ Either Stalin Hitler
data TCons2F r = TCons2F ShortText r r
  deriving (Functor, Show)

instance ShowType TCons2F where
  showTypeF (TCons2F name a b) = ShortText.intercalate " "
    [ name
    , a
    , b
    ]

-- | Type variable
-- @ a
data TVarF r = TVarF ShortText
  deriving (Functor, Show)

instance ShowType TVarF where
  showTypeF (TVarF name) = name

-- | Abstraction
-- @ a -> a -> Eq
data TArrowF r = TArrowF r r
  deriving (Functor, Show)

instance ShowType TArrowF where
  showTypeF (TArrowF a b) = mconcat
    [ a
    , " -> "
    , b
    ]

-- | Binary operation
-- @ name -> name -> name
binary
  :: (TArrowF :<: types, TVarF :<: types)
  => ShortText -> EADT types
binary name =
  VF $ TArrowF
    (VF var)
    ( VF $ TArrowF
      (VF var)
      (VF var)
    )
  where
    var = TVarF name

-- | Member the closed set
-- @ a || b -> c || D || ...
data TUnionF r = TUnionF [r]
  deriving (Functor, Show)

instance ShowType TUnionF where
  showTypeF (TUnionF xs) = ShortText.intercalate " || " xs

-- * Tree annotation

data TypedF types r = TypedF (EADT types) r
  deriving (Functor)

type Typed' types f = Compose (TypedF types) f
type TV' types cs = Typed' types (VariantF cs)

type Typed types f = Fix (Typed' types f)
type TV types cs = Typed types (VariantF cs)

class AnnTypes
  (f     :: Type -> Type)
  (types :: [Type -> Type])
  (cs    :: [Type -> Type])
  where
    annTypesF :: f (TV types cs) -> TV types cs

instance AnnTypes (VariantF '[]) types cs where
  annTypesF = undefined

instance
  ( AnnTypes (VariantF xs) types cs
  , AnnTypes x types cs
  ) => AnnTypes (VariantF (x ': xs)) types cs
  where
    annTypesF = either annTypesF annTypesF . popVariantFHead

instance
  ( Member ftv composed
  , PopVariant ftv composed
  , HasType types ftv
  , ftv ~ f (TV types cs)
  , composed ~ ApplyAll (TV types cs) cs
  ) => AnnTypes f types cs
  where
    annTypesF a =
      Fix . Compose $ TypedF (getType a) (FV a)

annTypes
  :: forall types cs.
    ( Functor (VariantF cs)
    , AnnTypes (VariantF cs) types cs
    )
  => EADT cs -> TV types cs
annTypes = cata annTypesF

instance
  ( TreeView1 (VariantF cs)
  , ShowType (VariantF types)
  , Functor (VariantF types)
  ) => TreeView1 (TV' types cs) where
  treeViewLabel1 (Compose (TypedF typ v)) = mconcat
    [ treeViewLabel1 v
    , " :: "
    , showType typ
    ]

  treeViewNodes1 (Compose (TypedF _ v)) =
    treeViewNodes1 v

treeViewTypes
  :: forall types cs.
    ( AnnTypes (VariantF cs) types cs
    , ShowType (VariantF types)
    , Functor (VariantF types)
    , TreeView1 (VariantF cs)
    , Functor (VariantF cs)
    )
  => EADT cs -> Tree String
treeViewTypes = cata treeViewF . annTypes @types

class ShowType (f :: Type -> Type) where
  showTypeF :: f ShortText -> ShortText

instance ShowType (VariantF '[]) where
  showTypeF = undefined

instance
  ( ShowType t
  , ShowType (VariantF ts)
  ) => ShowType (VariantF (t ': ts))
  where
    showTypeF = either showTypeF showTypeF . popVariantFHead

showType
  :: ( ShowType (VariantF ts)
     , Functor (VariantF ts)
     )
  => EADT ts -> String
showType = ShortText.unpack . cata showTypeF
