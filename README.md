# Variants that grow

* « Extensible ADT » by Sylvain Henry
  * blog: http://hsyl20.fr/home/posts/2018-05-22-extensible-adt.html
  * hackage: https://hackage.haskell.org/package/haskus-utils
    (a newer master is required)
* « Trees that grow » by Shayan Najd and Simon Peyton Jones
  * paper: http://www.jucs.org/jucs_23_1/trees_that_grow/jucs_23_01_0042_0062_najd.pdf
  * GHC wiki: https://ghc.haskell.org/trac/ghc/wiki/ImplementingTreesThatGrow

## Examples

(see `app/Main.hs`)

### Simple arithmetic

(defined in `src/Example/Arith.hs`)

EADT-style composite types, with TTG-style constructor annotations using
type families:

```haskell
-- | Addition-only
type IntSums x = EADT
  '[ ValF (XVal x) Int
   , AddF (XAdd x)
   ]

-- | Addition and multiplication
type IntMults x = EADT
  '[ ValF (XVal x) Int
   , AddF (XAdd x)
   , MulF (XMul x)
   ]
```

```haskell
x42 :: IntSums 'Generated
x42 = val @Int 42

y1337 :: IntSums 'Generated
y1337 = val @Int 1337

sumXY :: IntSums 'Generated
sumXY = x42 `add` y1337

multXYZ :: IntMults 'Generated
multXYZ = liftEADT sumXY `mul` val @Int 10
```

`VTG.Compiler.TreeView` instance, a `Show` alternative for ADTs:

```haskell
putStrLn $ treeView multXYZ
-- (*)
--  ├╴(+)
--  │  ├╴42
--  │  └╴1337
--  └╴10
```

`EvalInt` class and instances for Arith - evaluation with direct recursion:

```haskell
evalIntEADT multXYZ
-- 13790
```

### JSON

Classic JSON term:

```haskell
type JSON x = EADT (JSON' x)

type JSON' x =
  '[ NullF  (XNull  x)            -- null
   , LitF   (XLit   x) Scientific -- 4.2e1
   , LitF   (XLit   x) Text       -- "лꙐжи"
   , ArrayF (XArray x)            -- [ null, 4.2e1, "лꙐжи" ]
   , DictF  (XDict  x)            -- { "key": ["values"] }
   ]
```

Number arrays (and arrays of that):

```haskell
type Nums x = EADT
  '[ LitF   (XLit   x) Scientific -- 4.2e1
   , ArrayF (XArray x)            -- [ 4.2e1, 2.71, 3.14 ]
   ]
```

Polymorphic literals:

```haskell
j42 :: JSON 'Generated
j42 = litNum 4.2e1

jHello :: JSON 'Generated
jHello = litStr "hellow, orld"
```

Non-representable value, `Lit ()` is not a subtype of `JSON`:
```haskell
jUnit :: JSON 'Generated
jUnit = lit ()
```

### Adding `Arith` expressions to `JSON`

... and, done!

```haskell
import Example.Arith as Arith
import Example.JSON as JSON

type JSE' x =
  Arith.AddF (Arith.XAdd x) ':
  Arith.MulF (Arith.XMul x) ':
  JSON.JSON' x

type JSE x = EADT (JSE' x)
```

Behold the power of `liftEADT`!

```haskell
jDict :: JSON 'Generated
nums  :: Nums 'Generated
jse   :: JSE  'Generated

combined :: JSE 'Generated
combined = dict
  [ ("json", liftEADT jDict)
  , ("nums", liftEADT nums)
  , ("expr", jse)
  ]
```

All together now!

```
JSON:
Dict
 ├╴"json"
 │  └╴Dict
 │     ├╴"null"
 │     │  └╴Null
 │     ├╴"number"
 │     │  └╴Lit Scientific
 │     │     └╴42.0
 │     ├╴"string"
 │     │  └╴Lit Text
 │     │     └╴"hellow, orld"
 │     └╴"array"
 │        └╴Array
 │           ├╴Null
 │           ├╴Lit Scientific
 │           │  └╴42.0
 │           └╴Lit Text
 │              └╴"hellow, orld"
 ├╴"nums"
 │  └╴Array
 │     ├╴Lit Scientific
 │     │  └╴42.0
 │     ├╴Lit Scientific
 │     │  └╴2.71
 │     ├╴Lit Scientific
 │     │  └╴3.14
 │     └╴Array
 └╴"expr"
    └╴(*)
       ├╴Array
       │  ├╴(+)
       │  │  ├╴Lit Scientific
       │  │  │  └╴1.0
       │  │  └╴Lit Scientific
       │  │     └╴1.0
       │  ├╴(+)
       │  │  ├╴Lit Scientific
       │  │  │  └╴2.0
       │  │  └╴Lit Scientific
       │  │     └╴2.0
       │  ├╴(+)
       │  │  ├╴Lit Scientific
       │  │  │  └╴3.0
       │  │  └╴Lit Scientific
       │  │     └╴3.0
       │  ├╴(+)
       │  │  ├╴Lit Scientific
       │  │  │  └╴4.0
       │  │  └╴Lit Scientific
       │  │     └╴4.0
       │  └╴(+)
       │     ├╴Lit Scientific
       │     │  └╴5.0
       │     └╴Lit Scientific
       │        └╴5.0
       └╴Lit Scientific
          └╴5.0
```
