module VTG.Compiler.Common
  ( Pass(..)
    -- * Extension utils
  , NoExt(..)
    -- * Common re-exports
  , Type
    -- ** recursion-schemes
  , Base, Fix(..), Recursive, cata, unfix
    -- ** haskus-utils
  , Member, Variant, popVariantHead, toVariant, pattern V
  , EADT, VariantF, liftEADT, popVariantFHead, (:<:), pattern VF
  ) where

import Data.Functor.Foldable (Base, Fix(..), Recursive, cata, unfix)
import Data.Kind (Type)
import Haskus.Utils.Variant
  ( Member, Variant, popVariantHead, toVariant, pattern V
  )
import Haskus.Utils.EADT
  ( EADT, liftEADT, (:<:)
  , VariantF, popVariantFHead, pattern VF
  )

data Pass
  = Parsed
  | Resolved
  | Generated -- ^ Same as Parsed, but devoid of locations

data NoExt = NoExt
  deriving (Show)
